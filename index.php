<?php include("php/conexion.php") ?>
<!DOCTYPE>
<html>

<head>
    <title>Prueba de Conexión BD</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,900i&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/ionicons@4.5.10-1/dist/css/ionicons.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/css/mdb.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="css/estilos.css">
</head>

<body>
    <h1>Datos para Mostrar de BD</h1>
    <div class="container">
        <div class="row">
            <div class="col-4">
                <form method="POST" action="index.php">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="idCte">Id</label>
                            <input class="form-control" type="text" name="IdCte" id="ideCte">
                        </div>
                        <div class="form-group">
                            <label for="nombreCte">Nombre</label>
                            <input class="form-control" type="text" name="nombre" id="nombreCte">
                        </div>
                        <div class="form-group">
                            <label for="celCte">Telefono</label>
                            <input class="form-control" type="text" name="telefono" id="celCte">
                        </div>
                        <div class="form-group">
                            <label for="correoCte">Correo</label>
                            <input class="form-control" type="text" name="correo" id="correoCte">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" name="enviar" id="btnEnviar">Enviar</button>
                    <button type="submit" class="btn btn-secondary" name="consultar" id="btnConsultar">Consultar</button>
                    <button type="submit" class="btn btn-success" name="actualizar">Actualizar</button>
                    <button type="submit" class="btn btn-danger" name="borrar">Borrar</button>
                    <button type="submit" class="btn btn-warning" name="consultarTodo">Ver Todos</button>
                </form>
            </div>
            <div class="col-8">
                <table class="table table-striped">
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th>Acciones</th>
                    </tr>
                    <?php
                    include('php/conexion.php');
                    $id = '';
                    if (isset($_POST['consultar'])) {
                        $id = $_POST['IdCte'];
                        $existe = 0;

                        if ($id == "") {
                            echo "El Id es Obligatorio";
                        } else {
                            $resultado = mysqli_query($conexion, "SELECT * FROM $tabla_Cte WHERE id = '$id'");
                            while ($consulta = mysqli_fetch_array($resultado)) { ?>
                                <tr>
                                    <td><?php echo $consulta['id']; ?></td>
                                    <td><?php echo $consulta['nombre']; ?></td>
                                    <td><?php echo $consulta['telefono']; ?></td>
                                    <td><?php echo $consulta['correo']; ?></td>
                                    <?php $existe++ ?>;
                                    <td>
                                        <a href="edit_task.php?id=<?php echo $consulta['id'] ?>" class="btn btn-secondary">
                                            <i class="fas fa-marker"></i>
                                        </a>
                                        <a href="delete_task.php?id=<?php echo $consulta['id'] ?>" class="btn btn-danger">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>


                            <?php } ?>
                            <!-- Cierra While -->
                            <?php if ($existe == 0) {
                                        echo "El documento no existe";
                                    } ?>
                        <?php } ?>

                    <? } ?>
                    <!-- Cierra If -->


                    <!-- INSERTANDO REGISTRO -->
                    <?php
                        include('php/conexion.php');
                        if (isset($_POST['enviar'])) {

                            $nombre = $_POST['nombre'];
                            $telefono = $_POST['telefono'];
                            $correo = $_POST['correo'];

                            if($nombre == "" || $correo == ""){
                                echo "Los Campos son Obligatorios";
                            }else{
                                mysqli_query($conexion, "INSERT INTO $tabla_Cte (nombre, telefono, correo) VALUES  ('$nombre','$telefono','$correo')");
                                include("closeCon.php");
                            }
                        }


                        /* ACTUALIZAR REGISTRO */
                        if (isset($_POST['actualizar'])) {

                            $id = $_POST['IdCte'];
                            $nombre = $_POST['nombre'];
                            $telefono = $_POST['telefono'];
                            $correo = $_POST['correo'];

                            if($id == "" || $nombre == "" || $correo == "" || $telefono == ""){
                                echo "Los Campos son Obligatorios";
                            }else{
                                $existe = 0;
                                $resultado = mysqli_query($conexion, "SELECT * FROM $tabla_Cte WHERE id = '$id'");
                                while ($consulta = mysqli_fetch_array($resultado))
                                {
                                    $existe++;
                                }
                                if($existe == 0){
                                    echo "El Documento a Actualizar no Existe";
                                }else{
                                    $_UPDATE_SQL = "UPDATE $tabla_Cte SET
                                
                                    nombre = '$nombre',
                                    telefono = '$telefono',
                                    correo = '$correo'
    
                                    WHERE id = '$id'";
                                    mysqli_query($conexion,$_UPDATE_SQL);
                                    echo "Registro Actualizado Correctamente";
                                }
                            }
                            include('php/closeCon.php');
                        }

                      /*   BORRAR REGISTRO */

                      if(isset($_POST['borrar'])){
                        $id = $_POST['IdCte'];
                        $existe = 0;

                        if ($id == "") {
                            echo "El Id es Obligatorio";
                        }else{
                            $resultado = mysqli_query($conexion, "SELECT * FROM $tabla_Cte WHERE id = '$id'");
                            while ($consulta = mysqli_fetch_array($resultado)){
                                $existe++;
                            }
                            if($existe==0){
                                echo "El documento a borrar no existe";
                            }else{
                                $_DELETE_SQL = "DELETE FROM $tabla_Cte WHERE id = '$id'";
                                mysqli_query($conexion,$_DELETE_SQL);
                                echo "El documento se elimino satisfactoriamente";
                            }
                        }
                      }
                    ?>

                     <!--  CONSULTAR TODO -->
                    <?php
                    include('php/conexion.php');
                    $id = '';
                    if (isset($_POST['consultarTodo'])) {
                        $id = $_POST['IdCte'];
                        $existe = 0;

                            $resultado = mysqli_query($conexion, "SELECT * FROM $tabla_Cte");
                            while ($consulta = mysqli_fetch_array($resultado)) { ?>
                                <tr>
                                    <td><?php echo $consulta['id']; ?></td>
                                    <td><?php echo $consulta['nombre']; ?></td>
                                    <td><?php echo $consulta['telefono']; ?></td>
                                    <td><?php echo $consulta['correo']; ?></td>
                                    <?php $existe++ ?>
                                    <td>
                                        <a href="edit_task.php?id=<?php echo $consulta['id'] ?>" class="btn btn-secondary">
                                            <i class="fas fa-marker"></i>
                                        </a>
                                        <a href="delete_task.php?id=<?php echo $consulta['id'] ?>" class="btn btn-danger">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>


                            <?php } ?>
                            <!-- Cierra While -->
                            <?php 
                            if ($existe == 0) {
                                echo "El documento no existe";
                            } ?>

                    <? } ?>
                    <!-- Cierra If -->


                </table>
            </div>
        </div>
    </div>

</body>

</html>